﻿
namespace FilesImport;

public class ImportProvider
{
    public List<IImportHendler> handlers = new List<IImportHendler>();
    
    public ImportProvider()
    {
        handlers.Add(new ImportExcel());
        handlers.Add(new Importcsv());
        handlers.Add(new Importjson());
    }
    
    public IImportHendler GetHendler(string filePath)
    {
        foreach (var handler in handlers)
        {
            if (handler.CanHandle(filePath))
            {
                return handler;
            }
        }
        
        return null;
    }
}
