﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Importcsv.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
using Syncfusion.XlsIO;

namespace FilesImport;

public class ImportExcel : IImportHendler
{
    public bool CanHandle(string filePath)
    {
        return filePath.HasExtension(".xlsx");
    }

    public List<Item> Import(string filePath)
    {
        ExcelEngine excelEngine = new ExcelEngine();
        
        IApplication application = excelEngine.Excel;  
        application.DefaultVersion = ExcelVersion.Xlsx;

        var fstream = new FileStream(filePath, FileMode.Open);
        IWorkbook workbook = application.Workbooks.Open(fstream);
        IWorksheet worksheet = workbook.Worksheets[0];

        var result = new List<Item>();

        int index = 1;
    
        while (true)
        {
            if (worksheet.Range[$"A{index}"].Value == "")
            {
                break;
            
            }
            var item = new Item();
            item.Name = worksheet.Range[$"A{index}"].Value;
            item.Price = Convert.ToDouble(worksheet.Range[$"C{index}"].Value);
            item.Quantity = Convert.ToInt32(worksheet.Range[$"B{index}"].Value);
            result.Add(item);
            index++;
        }
        
        return result;
    }

}
