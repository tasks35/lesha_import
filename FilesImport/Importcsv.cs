﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Importcsv.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FilesImport;

public class Importcsv : IImportHendler
{
    public bool CanHandle(string filePath)
    {
        return filePath.HasExtension(".csv");
    }
    
    public List<Item> Import(string filePath)
    {
        string readText = File.ReadAllText(filePath);

        string[] subs = readText.Split("**");

        var result = new List<Item>();

        foreach (var sub in subs)
        {
            if (String.IsNullOrEmpty(sub.Trim()))
            {
                continue;
            }

            var item = new Item();
            var parts = sub.Split(',');
            item.Name = parts[0];
            item.Quantity = Convert.ToInt32(parts[1]);
            item.Price = Convert.ToDouble(parts[2]);
            result.Add(item);
        }

        return result;
    }

}
