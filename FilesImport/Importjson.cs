﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Importcsv.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace FilesImport;

public class Importjson : IImportHendler
{
    public bool CanHandle(string filePath)
    {
        return filePath.HasExtension(".json");
    }
    
    public List<Item> Import(string filePath)
    {
        string readText = File.ReadAllText(filePath);

        var result = new List<Item>();
        
        result = JsonConvert.DeserializeObject<List<Item>>(readText);

        return result;
    }

}
