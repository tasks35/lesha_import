﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extencion.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FilesImport;

public static class Extencions
{
    public static bool HasExtension(this string filePath, string extension)
    {
        string ext = System.IO.Path.GetExtension(filePath);
        
        if (ext == extension)
        {
            return true;
        }
        
        return false;
    }
}
