﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="fsfmsf.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FilesImport;

public class Item
{
    public string Name;
    
    public int Quantity;
    
    public double Price;
}
