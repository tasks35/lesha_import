﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImportHendler.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FilesImport;

public interface IImportHendler
{
    bool CanHandle(string filePath);
    
    List<Item> Import(string filePath);
}
