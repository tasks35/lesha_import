﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Importcsv.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Text.RegularExpressions;

namespace FilesImport;

public class Importone
{
    public bool CanHandle(string filePath)
    {
        return filePath.HasExtension(".one");
    }
    
    public List<CargoFacility> Import(string filePath)
    {
        string readText = File.ReadAllText(filePath);

        var result = new List<CargoFacility>();
        
        var sections = GetMatches(readText, @"Cargo Facilities:[\s\S]{1,}");
        
        var facilities = GetMatches(sections[0] + "\n\n", @"Name:(.|\n)*?\n\s*\n");

        foreach (var facility in facilities)
        {
            var a = new CargoFacility();
            a.Name = GetMatches(facility, @"Name:([\s\S]{1,}?)\n", 1)[0];
            
            result.Add(a);
        }
        
        return result;
    }

    private static List<string> GetMatches(string readText, string pattern, int group = 0)
    {
        var regex = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Multiline);
        var match = regex.Match(readText);
        var result = new List<string>();

        while (match.Success && match.Groups.Count > group)
        {
            result.Add(match.Groups[group].ToString());
            match = match.NextMatch();
        }
        
        return result;
    }
}
