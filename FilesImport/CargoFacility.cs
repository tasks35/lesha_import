﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CargoFacility.cs" company="ZigZag">
//     Copyright © ZigZag 2024
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FilesImport;

public class CargoFacility
{
    public string Name;
    
    public string PhoneNumbers;
    
    public string CallOutPhone;
    
    public string ParkingInstructions;
}
